import 'package:flutter/material.dart';

class SignUpButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.only(top: 160),
      onPressed: () {},
      child: Text(
        "Não possui uma conta? Cadastre-se.",
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          fontWeight: FontWeight.w300, 
          fontSize: 14, 
          color: Colors.white, 
          letterSpacing: 0.5
        ),
      ),
    );
  }
}
